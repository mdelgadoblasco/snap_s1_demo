# SNAP Sentinel-1 demo: Satellite data manipulation using SNAP within Jupyter Notebook
## Sentinel-1 flood detection exercise :Myanmar flood 2019/07
Run the application from Binder clicking to the logo here:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mdelgadoblasco%2Fsnap_s1_demo/master?urlpath=lab)

This has been created by Earth Observation Platform & Phi-Lab Engineering Support team (PLES team).
Any enquiry send an email to : PLES_management@rheagroup.com

This repository is licensed with : **[European Space Agency Community License – v2.4 Permissive](https://essr.esa.int/license/european-space-agency-community-license-v2-4-permissive)**

There are several notebooks which:
- main notebook: S1_flood_full which does the Sentinel-1 data pre-processing using SNAP and Flood detection using Band combination. 
- 2 more notebooks included in resources folder:
    - Data_downloader is able to download data from Copernicus Sentinel Data Hub (providing credentials)
    - Results_visualisation plots the results over a basemap for visual inspection.


The notebooks can be modified to download other S1 data and modified the provided parameters.

Launch the application from the logo above and enjoy!
